import codeeditorapi.data.interfaces.IUserRepository;
import codeeditorapi.logic.implementations.AuthLogic;
import codeeditorapi.shared.exceptions.AuthenticationException;
import codeeditorapi.shared.requests.LoginRequest;
import codeeditorapi.shared.requests.RegisterRequest;
import codeeditorapi.shared.responses.AuthRegisterResponse;
import codeeditorapi.shared.responses.LoginResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.security.PublicKey;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AuthLogicTest {
    @Mock
    private IUserRepository userRepository;
    @Spy
    private MockHttpLogic mockHttpLogic = new MockHttpLogic();
    @Spy
    private ObjectMapper objectMapper;
    @Mock
    private PublicKey publicKey;
    @InjectMocks
    private AuthLogic authLogic;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRegister() {
        AuthRegisterResponse response = new AuthRegisterResponse();
        response.setUserId("1234");
        this.mockHttpLogic.initMockResponse(200, response);
        RegisterRequest request = new RegisterRequest();
        request.setEmail("sjaak@email.nl");
        request.setName("sjaak");
        request.setPassword("groenteboer");
        assertDoesNotThrow(() -> authLogic.register(request));
    }

    @Test
    public void testLogin() {
        LoginResponse response = new LoginResponse();
        response.setToken("ey1234");
        this.mockHttpLogic.initMockResponse(200, response);

        LoginRequest request = new LoginRequest();
        request.setEmail("sjaak@email.nl");
        request.setPassword("groenteboer");

        assertDoesNotThrow(() -> authLogic.login(request));
    }

    @Test
    public void testLoginWrongCredentials() {
        mockHttpLogic.initMockResponse(401, null);

        LoginRequest request = new LoginRequest();
        request.setEmail("sjaak@email.nl");
        request.setPassword("visser");

        assertThrows(AuthenticationException.class, () -> authLogic.login(request));
    }
}
