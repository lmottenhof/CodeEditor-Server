import codeeditorapi.application.websocket.MessageHandler;
import codeeditorapi.application.websocket.MessageReceivedEventArgs;
import codeeditorapi.application.websocket.WebSocketHandler;
import codeeditorapi.application.websocket.messages.BaseMessage;
import codeeditorapi.logic.implementations.AuthLogic;
import codeeditorapi.logic.implementations.ProjectLogic;
import codeeditorapi.logic.implementations.UserLogic;
import codeeditorapi.shared.TokenData;
import codeeditorapi.shared.models.UserModel;
import codeeditorapi.shared.responses.FileResponse;
import codeeditorapi.shared.responses.ProjectDetailResponse;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.web.socket.BinaryMessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseHandlerTests {

    @Mock
    protected WebSocketHandler socket;
    @Mock
    protected AuthLogic authLogic;
    @Mock
    protected UserLogic userLogic;
    @Mock
    protected ProjectLogic projectLogic;

    protected UserModel sender = new UserModel();
    protected UserModel receiver = new UserModel();
    protected FileResponse file = new FileResponse();
    protected ProjectDetailResponse project = new ProjectDetailResponse();
    protected Map<Byte, MessageHandler> handlers = new HashMap<>();

    protected void setupUsers() throws Exception {



        sender.setId("1234");
        sender.setName("TestMeneer");
        sender.setEmail("test@email.com");

        receiver.setId("5678");
        receiver.setName("TestMevrouw");
        receiver.setEmail("test2@email.com");

        TokenData senderData = new TokenData();
        senderData.setEmail(sender.getEmail());
        senderData.setName(sender.getName());
        senderData.setId(sender.getId());

        TokenData receiverData = new TokenData();
        receiverData.setId(receiver.getId());
        receiverData.setName(receiver.getName());
        receiverData.setEmail(receiver.getEmail());

        Mockito.when(this.userLogic.getUserById(this.sender.getId())).thenReturn(this.sender);
        Mockito.when(this.userLogic.getUserById(this.receiver.getId())).thenReturn(this.receiver);

        Mockito.when(this.authLogic.verify(sender.getId())).thenReturn(senderData);
        Mockito.when(this.authLogic.verify(receiver.getId())).thenReturn(receiverData);
    }

    protected void setupProject() throws Exception {
        setupUsers();

        file.setPath("helloworld.c");

        project.setName("testProject");
        project.setProjectRoot("TestMeneer/testProject");
        project.setFiles(new FileResponse[]{file});

        Mockito.when(this.projectLogic.getProjectByName("testProject", "TestMeneer", receiver.getId())).thenReturn(this.project);

        Mockito.doAnswer((args) -> {
            handlers.put(args.getArgument(0, Byte.class), args.getArgument(1, MessageHandler.class));
            return null;
        }).when(this.socket).registerHandler(Mockito.anyByte(), Mockito.any());


    }

    protected void mockMessage(UserModel sender, BaseMessage message) throws Exception {
        MessageReceivedEventArgs eventArgs = new MessageReceivedEventArgs(message,
                sender,
                List.of(this.sender, receiver),
                project);
        Mockito.doAnswer((arg) -> {
            handlers.get(arg.getArgument(1, BinaryMessage.class).getPayload().get(0)).handle(eventArgs);
            return null;
        }).when(this.socket).handleMessage(Mockito.any(), Mockito.any());
        this.socket.handleMessage(null, new BinaryMessage(message.getBytes()));
    }
}
