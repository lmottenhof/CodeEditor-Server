import codeeditorapi.application.websocket.ByteUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ByteUtilTests {

    @Test
    public void testReadNumberFromBuffer() {
        //bytes in java are signed, for some reason
        byte[] bytes = new byte[]{0x00,  0x5C,  0x30, 0X7F};
        int number = ByteUtil.readInt(bytes, 0, 4);

        Assertions.assertEquals(0x005C307F, number);
    }
}
