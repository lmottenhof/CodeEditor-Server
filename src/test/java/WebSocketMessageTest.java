import codeeditorapi.application.websocket.messages.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import codeeditorapi.application.websocket.WebSocketHandler;

import java.util.Arrays;

public class WebSocketMessageTest {

    @Test
    public void testWriteChatMessage() {
        String senderName = "TestMeneer";
        String content = "Hallo";
        ChatMessage message = new ChatMessage("TestMeneer", "Hallo");
        byte[] bytes = message.getBytes();
        Assertions.assertEquals(WebSocketHandler.CHATHANDLER, bytes[0]);
        Assertions.assertEquals(senderName.length(), bytes[2]);
        Assertions.assertEquals(content.length(), bytes[13]);
        Assertions.assertArrayEquals(Arrays.copyOfRange(bytes, 3, 13), senderName.getBytes());
        Assertions.assertArrayEquals(Arrays.copyOfRange(bytes, 14, 19), content.getBytes());
    }

    @Test
    public void createBaseMessage() {
        byte messageType = 5;
        byte[] bytes = new byte[]{messageType,1,2};
        BaseMessage message = new BaseMessage(bytes);

        Assertions.assertEquals(messageType, message.getMessageType());
        Assertions.assertEquals(bytes, message.getBytes());
    }

    @Test
    public void createTextEditMessage() {
        byte[] bytes = new byte[]{1, 2, 0x00,0x00,0x00,0x05};
        BaseMessage base = new BaseMessage(bytes);

        TextEditMessage message = new TextEditMessage(base);


        Assertions.assertEquals(base.getBytes(), message.getBytes());
        Assertions.assertEquals(0x00000005, message.getEditPosition());
        Assertions.assertEquals(bytes[1], message.getEditType());
        Assertions.assertEquals(bytes[0], message.getMessageType());
    }

    @Test
    public void createTextInsertMessage() {
        byte[] bytes = new byte[]{1, 2, 0x00,0x00,0x00,0x05, 'H', 'E', 'L','L', 'O'};
        BaseMessage base = new BaseMessage(bytes);

        TextInsertMessage message = new TextInsertMessage(base);

        Assertions.assertEquals(base.getBytes(), message.getBytes());
        Assertions.assertEquals(0x00000005, message.getEditPosition());
        Assertions.assertEquals(bytes[1], message.getEditType());
        Assertions.assertEquals(bytes[0], message.getMessageType());
        Assertions.assertEquals("HELLO", message.getInsertedText());
    }

    @Test
    public void createTextRemoveMessage() {
        byte[] bytes = new byte[]{1, 2, 0x00,0x00,0x00,0x05, 5};
        BaseMessage base = new BaseMessage(bytes);

        TextRemoveMessage message = new TextRemoveMessage(base);

        Assertions.assertEquals(base.getBytes(), message.getBytes());
        Assertions.assertEquals(0x00000005, message.getEditPosition());
        Assertions.assertEquals(bytes[1], message.getEditType());
        Assertions.assertEquals(bytes[0], message.getMessageType());
        Assertions.assertEquals(5, message.getRemovedTextLength());

    }
}
