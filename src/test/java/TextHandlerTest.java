import codeeditorapi.application.websocket.MessageReceivedEventArgs;
import codeeditorapi.application.websocket.TextHandler;
import codeeditorapi.application.websocket.WebSocketHandler;
import codeeditorapi.application.websocket.MessageHandler;
import codeeditorapi.application.websocket.messages.BaseMessage;
import codeeditorapi.application.websocket.messages.TextEditMessage;
import codeeditorapi.application.websocket.messages.TextInsertMessage;
import codeeditorapi.application.websocket.messages.TextRemoveMessage;
import codeeditorapi.data.interfaces.IBufferedFileRepository;
import codeeditorapi.shared.models.UserModel;
import codeeditorapi.shared.responses.FileResponse;
import codeeditorapi.shared.responses.ProjectDetailResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.socket.BinaryMessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TextHandlerTest extends BaseHandlerTests {

    @Mock
    private IBufferedFileRepository fileRepo;

    @BeforeEach
    private void init() throws Exception {

        MockitoAnnotations.initMocks(this);

        this.setupProject();

        TextHandler handler = new TextHandler(this.socket, this.fileRepo);
        this.socket.registerHandler(WebSocketHandler.TEXTHANDLER, handler::handleMessage);
    }



    private void setupUserOpenedFile() throws Exception {
        byte[] messageBytes = new byte[2 + this.file.getPath().length()];
        messageBytes[0] = WebSocketHandler.TEXTHANDLER;
        messageBytes[1] = 2;
        System.arraycopy(this.file.getPath().getBytes(), 0, messageBytes, 2, this.file.getPath().getBytes().length);
        mockMessage(sender, new TextEditMessage(new BaseMessage(messageBytes)));
        mockMessage(receiver, new TextEditMessage(new BaseMessage(messageBytes)));
    }

    @Test
    public void testInsertText() throws Exception{
        this.setupUserOpenedFile();
        mockMessage(sender, new BaseMessage(new byte[]{1,0,0,0,0,5,'H', 'E', 'L','L','O'}));
        Mockito.verify(socket, Mockito.atLeastOnce()).registerHandler(Mockito.eq(WebSocketHandler.TEXTHANDLER), Mockito.any(MessageHandler.class));
        Mockito.verify(socket, Mockito.times(1)).sendMessage(Mockito.eq(receiver), Mockito.any(TextInsertMessage.class));
        Mockito.verify(this.fileRepo, Mockito.times(1)).write(project.getProjectRoot() + "/" + file.getPath(), 5, "HELLO");
    }

    @Test
    public void testRemoveText() throws Exception {
        this.setupUserOpenedFile();
        mockMessage(sender, new BaseMessage(new byte[]{1,1,0,0,0,5,5}));
        Mockito.verify(socket, Mockito.times(1)).sendMessage(Mockito.eq(receiver), Mockito.any(TextRemoveMessage.class));
        Mockito.verify(this.fileRepo, Mockito.times(1)).remove(project.getProjectRoot() + "/" + file.getPath(), 5, 5);
    }
}
