import codeeditorapi.logic.interfaces.IHttpLogic;
import org.mockito.Mockito;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;

public class MockHttpLogic implements IHttpLogic {
    private HttpResponse response = Mockito.mock(HttpResponse.class);

    public void initMockResponse(int responseCode, Object body) {
        Mockito.when(response.statusCode()).thenReturn(responseCode);
        Mockito.when(response.body()).thenReturn(body);
    }

    @Override
    public CompletableFuture<HttpResponse> requestAsync(String urlString, String requestType) throws IOException, URISyntaxException {
        return CompletableFuture.supplyAsync(() -> response);
    }

    @Override
    public CompletableFuture<HttpResponse> requestAsync(String urlString, String requestType, Object requestBody) throws IOException, URISyntaxException {
        return CompletableFuture.supplyAsync(() -> response);

    }

    @Override
    public <T> CompletableFuture<HttpResponse<T>> requestAsync(String urlString, String requestType, Class<T> clazz) throws IOException, URISyntaxException {
        //Mockito.when(response.body()).thenReturn((T)returnType);
        return CompletableFuture.<HttpResponse<T>>supplyAsync(() -> (HttpResponse<T>) response);
    }

    @Override
    public <T> CompletableFuture<HttpResponse<T>> requestAsync(String urlString, String requestType, Class<T> clazz, Object requestBody, String... headers) throws IOException, URISyntaxException {
        //Mockito.when(response.body()).thenReturn((T)returnType);
        return CompletableFuture.<HttpResponse<T>>supplyAsync(() -> (HttpResponse<T>)response);
    }
}
