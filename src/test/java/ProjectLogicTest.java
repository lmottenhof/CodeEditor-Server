import codeeditorapi.data.implementations.FileRepository;
import codeeditorapi.data.implementations.ProjectRepository;
import codeeditorapi.logic.implementations.AuthLogic;
import codeeditorapi.logic.implementations.ProjectLogic;
import codeeditorapi.logic.interfaces.IHttpLogic;
import codeeditorapi.shared.TokenData;
import codeeditorapi.shared.datatransferobjects.ProjectDTO;
import codeeditorapi.shared.exceptions.AuthenticationException;
import codeeditorapi.shared.exceptions.ResourceAlreadyExistsException;
import codeeditorapi.shared.exceptions.ResourceNotFoundException;
import codeeditorapi.shared.responses.FileDataResponse;
import codeeditorapi.shared.responses.ProjectDetailResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

public class ProjectLogicTest {

    @InjectMocks
    private ProjectLogic projectLogic;

    @Mock
    private FileRepository fileRepo;
    @Mock
    private ProjectRepository projectRepo;
    @Mock
    private AuthLogic authLogic;

    @BeforeEach
    private void init() throws AuthenticationException {
        MockitoAnnotations.initMocks(this);
        TokenData data = new TokenData();
        data.setId("1234");
        data.setName("TestMeneer");
        data.setEmail("test@email.com");
        Mockito.when(this.authLogic.verify(Mockito.anyString())).thenReturn(data);
    }

    @Test
    public void testNewProject() {
        Assertions.assertDoesNotThrow(() -> this.projectLogic.newProject("1234", "testProject", "TestMeneer"));
    }

    @Test
    public void testExistingProject() {
        Assertions.assertDoesNotThrow(() -> this.projectLogic.newProject("1234", "testProject", "TestMeneer"));
        ;
        Mockito.when(this.projectRepo.projectExists("testProject", "1234")).thenReturn(true);
        Assertions.assertThrows(ResourceAlreadyExistsException.class, () -> this.projectLogic.newProject("1234", "testProject", "TestMeneer"));
    }

    @Test
    public void testGetProjectByName() {
        ProjectDTO project = new ProjectDTO();
        project.setProjectRoot("TestMeneer/testProject");
        project.setName("testProject");
        project.setOwnerName("TestMeneer");
        project.setOwner("1234");
        Mockito.when(this.projectRepo.getProjectByName("testProject", "TestMeneer")).thenReturn(project);

        ProjectDetailResponse projectDetails = Assertions.assertDoesNotThrow(() -> this.projectLogic.getProjectByName("testProject", "TestMeneer", "1234"));

        Assertions.assertEquals("testProject", projectDetails.getName());
        Assertions.assertEquals( "TestMeneer/testProject", projectDetails.getProjectRoot());

    }

    @Test
    public void testGetProjectWrongCredentials() {
        ProjectDTO project = new ProjectDTO();
        project.setProjectRoot("TestMeneer/testProject");
        project.setName("testProject");
        project.setOwnerName("TestMeneer");
        project.setOwner("1234");

        Mockito.when(this.projectRepo.getProjectByName("testProject", "TestMeneer")).thenReturn(project);

        Assertions.assertThrows(AuthenticationException.class, () -> this.projectLogic.getProjectByName("testProject", "TestMeneer", "null"));
    }

    @Test
    public void testReadProjectFile() {
        String fileContent = "int main() {printf(\"Hello, World!\");}";
        try {
            Mockito.when(this.fileRepo.readFile("TestMeneer/testProject/hello.c")).thenReturn(fileContent);
        } catch (Exception e) {
            Assertions.fail("Incorrect test");
        }

        ProjectDTO project = new ProjectDTO();
        project.setOwner("1234");
        project.setOwnerName("TestMeneer");
        project.setProjectRoot("TestMeneer/testProject");
        project.setName("testProject");

        Mockito.when(this.projectRepo.getProjectsOfUser("1234")).thenReturn(List.of(project));

        FileDataResponse file = Assertions.assertDoesNotThrow(() -> this.projectLogic.getFile("testProject", "TestMeneer", "hello.c", "1234"));

        Assertions.assertEquals(fileContent, file.getContent());
    }

    @Test
    public void testReadNonExistingProjectFile() {
        Mockito.when(this.projectRepo.getProjectsOfUser("1234")).thenReturn(List.of());
        Assertions.assertThrows(ResourceNotFoundException.class, () -> this.projectLogic.getFile("testProject", "TestMeneer", "hello.c", "1234"));
    }
}
