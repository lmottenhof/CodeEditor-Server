import codeeditorapi.application.websocket.ChatHandler;
import codeeditorapi.application.websocket.TextHandler;
import codeeditorapi.application.websocket.WebSocketHandler;
import codeeditorapi.application.websocket.messages.BaseMessage;
import codeeditorapi.application.websocket.messages.ChatMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ChatHandlerTests extends BaseHandlerTests {

    @BeforeEach
    private void init() throws Exception {

        MockitoAnnotations.initMocks(this);

        this.setupProject();

        ChatHandler handler = new ChatHandler(this.socket);
        this.socket.registerHandler(WebSocketHandler.CHATHANDLER, handler::handleMessage);
    }

    @Test
    public void testSendChatMessage() throws Exception {
        mockMessage(sender, new BaseMessage(new byte[]{0,0,5 ,'H', 'E', 'L', 'L', 'O'}));
        Mockito.verify(socket, Mockito.times(1)).sendMessage(Mockito.eq(sender), Mockito.argThat((ChatMessage message) ->
                message.getSender().equals(sender.getName()) && message.getContent().equals("HELLO")
                ));
        Mockito.verify(socket, Mockito.times(1
        )).sendMessage(Mockito.eq(receiver), Mockito.argThat((ChatMessage message) ->
                message.getSender().equals(sender.getName()) && message.getContent().equals("HELLO")
        ));
    }

}
