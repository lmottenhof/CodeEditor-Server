import codeeditorapi.data.interfaces.IUserRepository;
import codeeditorapi.logic.implementations.UserLogic;
import codeeditorapi.shared.models.UserModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

public class UserLogicTests {
    @Mock
    private IUserRepository userRepo;

    @InjectMocks
    private UserLogic userLogic;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetById() {
        String userId = "1234";
        UserModel user = new UserModel();
        user.setEmail("test@email.com");
        user.setName("TestMeneer");
        user.setId(userId);
        Mockito.when(userRepo.getUserById(userId)).thenReturn(user);

        UserModel result = this.userLogic.getUserById("1234");
        Assertions.assertEquals(user.getId(), result.getId());
        Assertions.assertEquals(user.getName(), result.getName());
        Assertions.assertEquals(user.getEmail(), result.getEmail());

    }
}
