import codeeditorapi.application.websocket.MessageReceivedEventArgs;
import codeeditorapi.application.websocket.TextHandler;
import codeeditorapi.application.websocket.WebSocketHandler;
import codeeditorapi.application.websocket.messages.BaseMessage;
import codeeditorapi.application.websocket.messages.ChatMessage;
import codeeditorapi.application.websocket.messages.TextInsertMessage;
import codeeditorapi.logic.implementations.AuthLogic;
import codeeditorapi.logic.implementations.ProjectLogic;
import codeeditorapi.logic.implementations.UserLogic;
import codeeditorapi.logic.interfaces.IUserLogic;
import codeeditorapi.shared.TokenData;
import codeeditorapi.shared.models.UserModel;
import codeeditorapi.shared.responses.ProjectDetailResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.util.Map;

public class WebSocketHandlerTests extends BaseHandlerTests{


    @Mock
    private TextHandler mockHandler;

    @Mock
    private WebSocketSession senderSession;

    @Mock
    private WebSocketSession receiverSession;

    @InjectMocks
    private WebSocketHandler socketHandler;

    private void mockAuthentication() throws Exception {
        this.setupProject();
        Mockito.when(this.senderSession.getAttributes()).thenReturn(Map.of("projectRoot",this.project.getProjectRoot()));
        this.socketHandler.handleMessage(senderSession, new TextMessage(sender.getId()));
    }

    @BeforeEach
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.socketHandler.registerHandler(WebSocketHandler.TEXTHANDLER, mockHandler::handleMessage);

        this.setupUsers();
        mockAuthentication();
    }

    @Test
    public void testHandlerCalled() throws Exception {
        BaseMessage message = new BaseMessage(new byte[]{WebSocketHandler.TEXTHANDLER});
        this.socketHandler.handleMessage(this.senderSession, new BinaryMessage(message.getBytes()));
        Mockito.verify(mockHandler, Mockito.times(1)).handleMessage(Mockito.any());

    }
    @Test
    public void testAuthentication() throws Exception {


        this.socketHandler.handleMessage(senderSession, new TextMessage("1234"));
        Mockito.verify(authLogic, Mockito.times(1)).verify("1234");
    }

    @Test
    public void testDifferentProjectUser() throws Exception {

        Mockito.when(this.senderSession.getAttributes()).thenReturn(Map.of("projectRoot", this.project.getProjectRoot()));
        Mockito.when(this.receiverSession.getAttributes()).thenReturn(Map.of("projectRoot", "different/project"));
        ProjectDetailResponse differentProject = new ProjectDetailResponse();

        Mockito.when(this.projectLogic.getProjectByName("project", "different", receiver.getId())).thenReturn(differentProject);

        this.socketHandler.handleMessage(senderSession, new TextMessage("1234"));
        this.socketHandler.handleMessage(receiverSession, new TextMessage("5678"));

        this.socketHandler.handleMessage(senderSession, new BinaryMessage((new byte[]{1,0,'H','E','L', 'L', 'O'})));

        Mockito.verify(this.mockHandler, Mockito.times(1)).handleMessage(Mockito.argThat((MessageReceivedEventArgs message) ->
                !message.getUsers().contains(receiver)
        ));
    }
}
