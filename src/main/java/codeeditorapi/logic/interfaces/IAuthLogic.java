package codeeditorapi.logic.interfaces;

import codeeditorapi.shared.TokenData;
import codeeditorapi.shared.exceptions.AuthenticationException;
import codeeditorapi.shared.exceptions.RegisterException;
import codeeditorapi.shared.requests.LoginRequest;
import codeeditorapi.shared.requests.RegisterRequest;
import codeeditorapi.shared.responses.LoginResponse;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

public interface IAuthLogic {
    LoginResponse login(LoginRequest request) throws IOException, URISyntaxException, InterruptedException, ExecutionException, AuthenticationException;
    void register(RegisterRequest request) throws IOException, URISyntaxException, InterruptedException, ExecutionException, RegisterException;
    TokenData verify(String token) throws AuthenticationException;
 }
