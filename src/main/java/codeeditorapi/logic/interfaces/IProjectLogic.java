package codeeditorapi.logic.interfaces;

import codeeditorapi.shared.exceptions.AuthenticationException;
import codeeditorapi.shared.exceptions.ResourceAlreadyExistsException;
import codeeditorapi.shared.responses.FileDataResponse;
import codeeditorapi.shared.responses.ProjectDetailResponse;
import codeeditorapi.shared.responses.ProjectResponse;

import java.io.IOException;
import java.util.List;

public interface IProjectLogic {
    String newProject(String ownerId, String projectName, String userName) throws ResourceAlreadyExistsException;
    ProjectDetailResponse getProjectByName(String name, String userName, String userId) throws AuthenticationException;
    List<ProjectResponse> getProjectsOfUser(String userId);
    FileDataResponse getFile(String projectName, String userName, String path, String userId) throws IOException;
    void saveFile(byte[] content, String projectName, String userName, String path, String userId) throws IOException;
}