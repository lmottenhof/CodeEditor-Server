package codeeditorapi.logic.interfaces;

import org.springframework.lang.Nullable;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;

public interface IHttpLogic {
    <T> CompletableFuture<HttpResponse<T>> requestAsync(String urlString, String requestType) throws IOException, URISyntaxException;
    <T> CompletableFuture<HttpResponse<T>> requestAsync(String urlString, String requestType, @Nullable Object requestBody) throws IOException, URISyntaxException;
    <T> CompletableFuture<HttpResponse<T>> requestAsync(String urlString, String requestType, @Nullable Class<T> clazz) throws IOException, URISyntaxException;
    <T> CompletableFuture<HttpResponse<T>> requestAsync(String urlString, String requestType, @Nullable Class<T> clazz, @Nullable Object requestBody, String... headers) throws IOException, URISyntaxException;
}
