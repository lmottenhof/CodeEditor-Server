package codeeditorapi.logic.interfaces;

import codeeditorapi.shared.models.UserModel;

public interface IUserLogic {
    UserModel getUserById(String id);
}
