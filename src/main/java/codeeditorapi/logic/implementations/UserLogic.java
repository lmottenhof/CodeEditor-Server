package codeeditorapi.logic.implementations;

import codeeditorapi.data.interfaces.IUserRepository;
import codeeditorapi.logic.interfaces.IUserLogic;
import codeeditorapi.shared.models.UserModel;


public class UserLogic implements IUserLogic {

    private final IUserRepository userRepo;

    public UserLogic(IUserRepository userRepo) {
        this.userRepo = userRepo;
    }

    public UserModel getUserById(String id) {
        return this.userRepo.getUserById(id);
    }
}
