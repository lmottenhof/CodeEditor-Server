package codeeditorapi.logic.implementations;


import codeeditorapi.data.interfaces.IUserRepository;
import codeeditorapi.logic.interfaces.IAuthLogic;
import codeeditorapi.logic.interfaces.IHttpLogic;
import codeeditorapi.shared.TokenData;
import codeeditorapi.shared.config.Endpoints;
import codeeditorapi.shared.datatransferobjects.UserDTO;
import codeeditorapi.shared.exceptions.AuthenticationException;
import codeeditorapi.shared.exceptions.RegisterException;
import codeeditorapi.shared.requests.LoginRequest;
import codeeditorapi.shared.requests.RegisterRequest;
import codeeditorapi.shared.responses.AuthRegisterResponse;
import codeeditorapi.shared.responses.LoginResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.JacksonDeserializer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;
import java.security.PublicKey;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class AuthLogic implements IAuthLogic {
    private final IHttpLogic httpLogic;
    private final IUserRepository userRepo;
    private final PublicKey publicKey;
    private final ObjectMapper objectMapper;

    public AuthLogic(IHttpLogic httpLogic, IUserRepository userRepo, ObjectMapper objectMapper, PublicKey key) {
        this.httpLogic = httpLogic;
        this.userRepo = userRepo;
        this.publicKey = key;
        this.objectMapper = objectMapper;
    }

    public LoginResponse login(LoginRequest request) throws IOException, URISyntaxException, InterruptedException, ExecutionException, AuthenticationException {

        CompletableFuture<HttpResponse<LoginResponse>> completableLoginResponse = this.httpLogic.requestAsync(Endpoints.LOGIN, HttpMethod.POST, LoginResponse.class, request);
        HttpResponse<LoginResponse> loginResponse = completableLoginResponse.get();
        if (loginResponse.statusCode() == 200) {
            return loginResponse.body();
        }
        throw new AuthenticationException("Failed to authenticate");
    }

    public void register(RegisterRequest request) throws IOException, URISyntaxException, InterruptedException, ExecutionException, RegisterException {
        CompletableFuture<HttpResponse<AuthRegisterResponse>> completableRegisterResponse = this.httpLogic.requestAsync(Endpoints.REGISTER, HttpMethod.POST, AuthRegisterResponse.class, request);
        UserDTO user = new UserDTO();
        user.setEmail(request.getEmail());
        user.setName(request.getName());

        HttpResponse<AuthRegisterResponse> registerResponse = completableRegisterResponse.get();
        if (registerResponse.statusCode() == 200) {
            user.setId(registerResponse.body().getUserId());
            this.userRepo.register(user);
            return;
        }
        throw new RegisterException("Email or name already in use");

    }

    public TokenData verify(String token) throws AuthenticationException{
        Claims claims;
        try {
           claims = Jwts.parser()
                    .deserializeJsonWith(new JacksonDeserializer<>(this.objectMapper))
                    .setSigningKey(this.publicKey)
                    .parseClaimsJws(token)
                    .getBody();
        } catch(Exception ex) {
            throw new AuthenticationException("Invalid token", ex);
        }

        TokenData data = new TokenData();
        data.setEmail(claims.get("email", String.class));
        data.setId(claims.get("id", String.class));
        data.setName(claims.get("name", String.class));
        return data;
    }
}
