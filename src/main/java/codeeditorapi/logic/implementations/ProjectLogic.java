package codeeditorapi.logic.implementations;

import codeeditorapi.data.interfaces.IFileRepository;
import codeeditorapi.data.interfaces.IProjectRepository;
import codeeditorapi.logic.interfaces.IProjectLogic;
import codeeditorapi.shared.config.Endpoints;
import codeeditorapi.shared.datatransferobjects.ProjectDTO;
import codeeditorapi.shared.datatransferobjects.ProjectFileDTO;
import codeeditorapi.shared.exceptions.AuthenticationException;
import codeeditorapi.shared.exceptions.ResourceAlreadyExistsException;
import codeeditorapi.shared.exceptions.ResourceNotFoundException;
import codeeditorapi.shared.responses.FileDataResponse;
import codeeditorapi.shared.responses.FileResponse;
import codeeditorapi.shared.responses.ProjectDetailResponse;
import codeeditorapi.shared.responses.ProjectResponse;
import mapping.MapFactory;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProjectLogic implements IProjectLogic {

    private final IProjectRepository projectRepo;
    private final IFileRepository fileRepo;
    private final Function<ProjectDTO, ProjectResponse> projectMapFunc = MapFactory.getDefaultMapper(ProjectResponse.class, ProjectDTO.class).getMapFunction();
    private final Function<ProjectFileDTO, FileResponse> projectFileMapFunc = MapFactory.getDefaultMapper(FileResponse.class, ProjectFileDTO.class).getMapFunction();
    private static final String PROJECT_PATH_TEMPLATE = "%s/%s/%s";

    public ProjectLogic(IProjectRepository projectRepo, IFileRepository fileRepo) {
        this.projectRepo = projectRepo;
        this.fileRepo = fileRepo;
    }

    @Override
    public String newProject(String ownerId, String projectName, String userName) throws ResourceAlreadyExistsException {
        if(this.projectRepo.projectExists(projectName, ownerId)) {
            throw new ResourceAlreadyExistsException(String.format("%s already has project with name: %s", userName, projectName));
        }

        ProjectDTO project = new ProjectDTO();
        String projectId = UUID.randomUUID().toString();
        project.setOwnerName(projectId);
        project.setOwner(ownerId);
        project.setName(projectName);
        project.setProjectRoot(String.format("%s/%s", userName, projectName));
        this.projectRepo.newProject(project);
        return projectId;
    }

    @Override
    public ProjectDetailResponse getProjectByName(String projectName, String userName, String userId) throws AuthenticationException {
        ProjectDTO project = this.projectRepo.getProjectByName(projectName, userName);
        if(project.getOwner().equals(userId) || this.projectRepo.getCollaboratingProjects(userId).contains(project)) {
            List<ProjectFileDTO> files = this.projectRepo.getFiles(project.getId());
            ProjectDetailResponse response = new ProjectDetailResponse();
            response.setName(project.getName());
            response.setProjectRoot(project.getProjectRoot());
            response.setFiles(files.stream().map(this.projectFileMapFunc).toArray(FileResponse[]::new));
            return response;
        }

        throw new AuthenticationException("Not found");
    }

    @Override
    public List<ProjectResponse> getProjectsOfUser(String userId){
       List<ProjectDTO> projects = this.projectRepo.getProjectsOfUser(userId);
       return projects.stream().map(this.projectMapFunc).collect(Collectors.toList());
    }

    public FileDataResponse getFile(String projectName, String userName, String path, String userId) throws IOException {
        String projectRoot = String.format("%s/%s", userName, projectName);
        List<ProjectDTO> projects = this.projectRepo.getProjectsOfUser(userId);

        if(projects.stream().anyMatch(p -> p.getProjectRoot().equals(projectRoot))) {
            String content = this.fileRepo.readFile(String.format(PROJECT_PATH_TEMPLATE, userName, projectName, path));
            FileDataResponse response = new FileDataResponse();
            response.setContent(content);
            return response;
        } else {
            throw new ResourceNotFoundException(String.format("%s/%s not found for user %s", projectRoot, path, userName));
        }
    }

    public void saveFile(byte[] content, String projectName, String userName, String path, String userId) throws IOException {
        String projectRoot = String.format("%s/%s", userName, projectName);
        List<ProjectDTO> projects = this.projectRepo.getProjectsOfUser(userId);
        Optional<ProjectDTO> optProject= projects.stream().filter(p -> p.getProjectRoot().equals(projectRoot)).findFirst();
        if(optProject.isPresent()) {
            String projectId = optProject.get().getId();
            this.projectRepo.addFileToProject(projectId, path);
            this.fileRepo.saveFile(content ,String.format(PROJECT_PATH_TEMPLATE, userName, projectName, path));
        } else {
            throw new ResourceNotFoundException(String.format("%s/%s not found for user %s", projectRoot, path, userName));
        }
    }
}
