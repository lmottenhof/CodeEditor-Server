package codeeditorapi.data.implementations;

import codeeditorapi.data.interfaces.IProjectRepository;
import codeeditorapi.shared.datatransferobjects.ProjectDTO;
import codeeditorapi.shared.datatransferobjects.ProjectFileDTO;
import mapping.springjdbcmapping.SpringJdbcMapFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

public class ProjectRepository implements IProjectRepository {
    private final JdbcTemplate dbc;
    private final RowMapper<ProjectDTO> projectMapper = SpringJdbcMapFactory.getRowMapper(ProjectDTO.class);
    private final RowMapper<ProjectFileDTO> projectFileMapper = SpringJdbcMapFactory.getRowMapper(ProjectFileDTO.class);

    public ProjectRepository(JdbcTemplate dbc) {
        this.dbc = dbc;
    }

    @Override
    public ProjectDTO getProjectById(String projectId) {
        return this.dbc.query("CALL getProjectById(?)", this.projectMapper, projectId).get(0);
    }

    @Override
    public List<ProjectDTO> getProjectsOfUser(String userId) {
        return this.dbc.query("CALL getProjectsOfUser(?)", projectMapper, userId);
    }

    @Override
    public List<ProjectDTO> getOwnedProjects(String userId) {
        return this.dbc.query("CALL getOwnedProjects(?)", projectMapper, userId);
    }

    @Override
    public List<ProjectDTO> getCollaboratingProjects(String userId) {
        return this.dbc.query("CALL getCollaboratingProjects(?)", projectMapper, userId);
    }

    @Override
    public ProjectDTO getProjectByName(String name, String userName) {
        return this.dbc.query("CALL getProjectByName(?, ?)", projectMapper, name, userName).get(0);
    }

    @Override
    public boolean projectExists(String projectName, String userId) {
        return (this.dbc.queryForObject("CALL projectExists(?,?)", Integer.class, projectName, userId) > 0);

    }

    @Override
    public void newProject(ProjectDTO project) {
        this.dbc.update("CALL newProject(?, ?, ?, ?)", project.getOwnerName(), project.getName(), project.getOwner(), project.getProjectRoot());
    }

    @Override
    public List<ProjectFileDTO> getFiles(String projectId) {
        return this.dbc.query("CALL getProjectFiles(?)",this.projectFileMapper, projectId);
    }

    @Override
    public void addFileToProject(String projectId, String path) {
        this.dbc.update("CALL addFileToProject(?,?)", projectId, path);
    }
}