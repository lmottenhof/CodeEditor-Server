package codeeditorapi.data.implementations;

import codeeditorapi.data.interfaces.IUserRepository;
import codeeditorapi.shared.datatransferobjects.UserDTO;
import codeeditorapi.shared.datatransferobjects.UserSettingsDTO;
import codeeditorapi.shared.models.UserModel;
import mapping.springjdbcmapping.SpringJdbcMapFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

public class UserRepository implements IUserRepository {

    private final JdbcTemplate dbContext;
    private final RowMapper<UserDTO> userMapper = SpringJdbcMapFactory.getRowMapper(UserDTO.class);
    private final RowMapper<UserSettingsDTO> userSettingsMapper = SpringJdbcMapFactory.getRowMapper(UserSettingsDTO.class);

    public UserRepository(JdbcTemplate template) {
        this.dbContext = template;
    }

    @Override
    public UserModel getUserById(String userId) {
        UserDTO user = this.dbContext.query("CALL getUserById(?)", userMapper, userId).get(0);
        UserSettingsDTO userSettings = this.dbContext.query("SELECT * FROM `UserSettings` WHERE UserId = ?", userSettingsMapper, userId).get(0);
        return new UserModel(user, userSettings);
    }

    @Override
    public List<UserDTO> getProjectUsers(String projectId) {
        return this.dbContext.query("CALL getProjectUsers(?)", userMapper, projectId);
    }

    @Override
    public boolean isRegistered(String email) {
        return false;
    }

    @Override
    public void register(UserDTO user) {
        this.dbContext.update("CALL registerUser(?,?,?)", user.getId(), user.getEmail(), user.getName());
    }

    @Override
    public void editUserSettings(UserSettingsDTO userSettings) {
        this.dbContext.update("CALL editUserSettings(?)", userSettings.getUserId());
    }
}
