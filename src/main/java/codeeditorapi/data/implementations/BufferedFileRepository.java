package codeeditorapi.data.implementations;

import codeeditorapi.data.interfaces.IBufferedFileRepository;
import codeeditorapi.data.interfaces.IFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BufferedFileRepository implements IBufferedFileRepository {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private IFileRepository innerRepo;
    private Map<String, StringBuilder> builders = new ConcurrentHashMap<>();
    private ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    public BufferedFileRepository(IFileRepository innerRepo) {
        this.innerRepo = innerRepo;

        Runnable saveTask = () ->
            this.builders.forEach((file, builder)  -> {
                try {
                    this.commitChanges(file);
                   logger.info("Saving buffered changes");
                } catch (IOException e) {
                    logger.error("Failed to save changes", e);
                }
            });

        this.scheduler.scheduleWithFixedDelay(saveTask, 1, 1, TimeUnit.MINUTES);
    }

    @Override
    public void saveFile(byte[] content, String fileName) throws IOException {
        this.innerRepo.saveFile(content, fileName);
    }

    @Override
    public String readFile(String fileName) throws IOException {
        if(this.builders.containsKey(fileName)) {
            return this.builders.get(fileName).toString();
        }
        else {
            return this.innerRepo.readFile(fileName);
        }
    }

    @Override
    public void write(String file, int pos, String str) throws IOException{
        if(!this.builders.containsKey(file)) {
            this.builders.put(file, new StringBuilder(this.innerRepo.readFile(file)));
        }

        this.builders.get(file).insert(pos, str);
    }

    @Override
    public void remove(String file, int pos, int count) throws IOException {
        if(!this.builders.containsKey(file)) {
            this.builders.put(file, new StringBuilder(this.innerRepo.readFile(file)));
        }
        this.builders.get(file).delete(pos, pos+count);
    }

    private void commitChanges(String file) throws IOException {
        StringBuilder builder = this.builders.get(file);
        this.saveFile(builder.toString().getBytes(), file);
    }

    @Override
    public void close(String file) throws IOException{
        if(this.builders.containsKey(file)) {
            commitChanges(file);
            this.builders.remove(file);
        }
    }
}
