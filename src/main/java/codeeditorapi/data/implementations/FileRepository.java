package codeeditorapi.data.implementations;

import codeeditorapi.data.interfaces.IFileRepository;
import codeeditorapi.shared.exceptions.ResourceNotFoundException;

import java.io.*;

public class FileRepository implements IFileRepository {

    private final String location;

    public FileRepository(String location) {
        this.location = location;

    }

    @Override
    public void saveFile(byte[] content, String fileName) throws IOException {
        String path = String.format("%s/%s", location, fileName);

        File file = new File(path);
        file.getParentFile().mkdirs();

        if(file.exists() || file.createNewFile()) {
            try (FileWriter writer = new FileWriter(file, false)) {
                writer.write(new String(content));
            }
        } else throw new IOException("Failed to create new file");

    }

    @Override
    public String readFile(String fileName) throws IOException {
        String path = String.format("%s/%s", location, fileName);
        File file = new File(path);
        if (file.exists()) {
            try (FileInputStream reader = new FileInputStream(file);) {
                return new String(reader.readAllBytes());
            }
        } else {
            throw new ResourceNotFoundException(String.format("File not found at %s", fileName));
        }
    }
}


