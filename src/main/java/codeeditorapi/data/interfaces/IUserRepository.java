package codeeditorapi.data.interfaces;

import codeeditorapi.shared.datatransferobjects.UserDTO;
import codeeditorapi.shared.datatransferobjects.UserSettingsDTO;
import codeeditorapi.shared.models.UserModel;

import java.util.List;

public interface IUserRepository {
    UserModel getUserById(String userId);
    List<UserDTO> getProjectUsers(String projectId);
    boolean isRegistered(String email);
    void register(UserDTO user);
    void editUserSettings(UserSettingsDTO userSettings);



}
