package codeeditorapi.data.interfaces;

import java.io.IOException;

public interface IFileRepository {
    void saveFile(byte[] content, String fileName) throws IOException;
    String readFile(String fileName) throws IOException;
}
