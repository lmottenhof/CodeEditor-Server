package codeeditorapi.data.interfaces;

import java.io.IOException;

public interface IBufferedFileRepository extends IFileRepository {
    void write(String file, int pos, String str) throws IOException;
    void remove(String file, int pos, int count) throws IOException;
    void close(String file) throws IOException;
}
