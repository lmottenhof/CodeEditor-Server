package codeeditorapi.data.interfaces;

import codeeditorapi.shared.datatransferobjects.ProjectDTO;
import codeeditorapi.shared.datatransferobjects.ProjectFileDTO;

import java.util.List;

public interface IProjectRepository {
    ProjectDTO getProjectById(String projectId);
    List<ProjectDTO> getProjectsOfUser(String userId);
    List<ProjectDTO> getOwnedProjects(String userId);
    List<ProjectDTO> getCollaboratingProjects(String userId);
    ProjectDTO getProjectByName(String name, String userName);
    boolean projectExists(String name, String userId);
    void newProject(ProjectDTO project);
    List<ProjectFileDTO> getFiles(String projectId);
    void addFileToProject(String projectId, String path);
}
