package codeeditorapi.shared.requests;

import lombok.Data;

@Data
public class NewProjectRequest {
    private String projectName;
}
