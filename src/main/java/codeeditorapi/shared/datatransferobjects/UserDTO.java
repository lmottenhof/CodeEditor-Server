package codeeditorapi.shared.datatransferobjects;

import lombok.Data;

@Data
public class UserDTO {
    private String id;
    private String name;
    private String email;
}
