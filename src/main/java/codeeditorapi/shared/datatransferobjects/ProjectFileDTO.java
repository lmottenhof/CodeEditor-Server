package codeeditorapi.shared.datatransferobjects;


import lombok.Data;

@Data
public class ProjectFileDTO {
    private String path;
}
