package codeeditorapi.shared.datatransferobjects;

import lombok.Data;

@Data
public class UserSettingsDTO {
    private String id;
    private String userId;
}
