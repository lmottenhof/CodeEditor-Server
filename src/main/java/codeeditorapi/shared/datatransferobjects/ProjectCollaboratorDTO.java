package codeeditorapi.shared.datatransferobjects;

import lombok.Data;

@Data
public class ProjectCollaboratorDTO {
    private String userId;
    private String projectId;
}
