package codeeditorapi.shared.datatransferobjects;

import lombok.Data;

@Data
public class ProjectSettingsDTO {
    private String id;
    private String projectId;
}
