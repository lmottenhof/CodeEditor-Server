package codeeditorapi.shared.datatransferobjects;

import lombok.Data;

import java.util.Objects;

@Data
public class ProjectDTO {
    private String id;
    private String ownerName;
    private String name;
    private String projectRoot;
    private String owner;

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof ProjectDTO)) {
            return false;
        }
        return ((ProjectDTO) o).projectRoot.equals(this.projectRoot);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectRoot);
    }
}
