package codeeditorapi.shared.models;

import codeeditorapi.shared.datatransferobjects.ProjectSettingsDTO;
import codeeditorapi.shared.datatransferobjects.UserDTO;
import lombok.Data;

import java.util.List;

@Data
public class ProjectModel {
    private String id;
    private ProjectSettingsDTO projectSettings;
    private String projectRoot;
    private String owner;
    private List<UserDTO> collaborators;

}
