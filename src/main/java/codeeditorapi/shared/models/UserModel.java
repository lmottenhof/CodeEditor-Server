package codeeditorapi.shared.models;

import codeeditorapi.shared.datatransferobjects.UserDTO;
import codeeditorapi.shared.datatransferobjects.UserSettingsDTO;
import lombok.Getter;
import lombok.Setter;

public class UserModel {
    @Getter @Setter
    private String id;
    @Getter@Setter
    private String name;
    @Getter @Setter
    private UserSettingsModel userSettings;
    @Getter @Setter
    private String email;

    public UserModel() {}
    public UserModel(UserDTO user, UserSettingsDTO settings) {
        this();
        this.id = user.getId();
        this.name = user.getName();
        this.userSettings = new UserSettingsModel(settings);
        this.email = user.getEmail();
    }

}
