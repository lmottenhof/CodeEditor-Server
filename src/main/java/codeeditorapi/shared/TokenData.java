package codeeditorapi.shared;

import lombok.Data;

@Data
public class TokenData {
    private String id;
    private String email;
    private String name;
}
