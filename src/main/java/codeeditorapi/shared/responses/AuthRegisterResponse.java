package codeeditorapi.shared.responses;

import lombok.Data;

@Data
public class AuthRegisterResponse {
    private String userId;
}
