package codeeditorapi.shared.responses;

import lombok.Data;

@Data
public class NewProjectResponse {
    private String id;
    private String name;
    private String userName;
}
