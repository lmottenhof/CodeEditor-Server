package codeeditorapi.shared.responses;

import lombok.Data;

@Data
public class ProjectDetailResponse {
    private String name;
    private String projectRoot;
    private FileResponse[] files;
}