package codeeditorapi.shared.responses;

import lombok.Data;

@Data
public class ProjectResponse {
    private String name;
    private String ownerName;
    private String projectRoot;
}