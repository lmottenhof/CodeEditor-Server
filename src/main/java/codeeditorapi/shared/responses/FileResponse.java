package codeeditorapi.shared.responses;

import lombok.Data;

@Data
public class FileResponse {
    private String path;
}
