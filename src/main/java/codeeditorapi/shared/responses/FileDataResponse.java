package codeeditorapi.shared.responses;

import lombok.Data;

@Data
public class FileDataResponse {
    private String content;
}

