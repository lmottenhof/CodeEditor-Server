package codeeditorapi.shared.responses;

import lombok.Data;

@Data
public class PublicKeyResponse {
    private String publicKey;
}
