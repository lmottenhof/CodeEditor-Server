package codeeditorapi.application;

import codeeditorapi.logic.interfaces.IAuthLogic;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    private IAuthLogic authLogic;
    public InterceptorConfig(IAuthLogic authLogic) {
        this.authLogic = authLogic;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthInterceptor(this.authLogic)).excludePathPatterns("/user/login", "/user/register");
    }
}