package codeeditorapi.application.controllers;

import codeeditorapi.logic.implementations.AuthLogic;
import codeeditorapi.logic.interfaces.IAuthLogic;
import codeeditorapi.shared.exceptions.AuthenticationException;
import codeeditorapi.shared.exceptions.RegisterException;
import codeeditorapi.shared.requests.LoginRequest;
import codeeditorapi.shared.requests.RegisterRequest;
import codeeditorapi.shared.responses.LoginResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@CrossOrigin("*")
public class UserController {
    private final IAuthLogic authLogic;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public UserController(AuthLogic authLogic) {
        this.authLogic = authLogic;
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest request) {
        LoginResponse jwt;
        try {
            jwt = this.authLogic.login(request);
        }
        catch(AuthenticationException ex) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception ex) {
            this.logger.info(String.format("Registering failed for %s", request.getEmail()), ex);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(jwt, HttpStatus.OK);
    }


    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody RegisterRequest request) {
        try {
            this.authLogic.register(request);
        } catch(RegisterException ex) {
            return new ResponseEntity<>(ex.getMessage(),HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            this.logger.info(String.format("Registering failed for %s with %s", request.getName(), request.getEmail()), ex);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
