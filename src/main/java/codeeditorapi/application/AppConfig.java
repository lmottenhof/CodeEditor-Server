package codeeditorapi.application;

import codeeditorapi.data.implementations.BufferedFileRepository;
import codeeditorapi.data.implementations.FileRepository;
import codeeditorapi.data.implementations.ProjectRepository;
import codeeditorapi.data.implementations.UserRepository;
import codeeditorapi.data.interfaces.IBufferedFileRepository;
import codeeditorapi.data.interfaces.IFileRepository;
import codeeditorapi.data.interfaces.IProjectRepository;
import codeeditorapi.data.interfaces.IUserRepository;
import codeeditorapi.logic.implementations.*;
import codeeditorapi.logic.interfaces.IProjectLogic;
import codeeditorapi.shared.config.Endpoints;
import codeeditorapi.shared.responses.PublicKeyResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.concurrent.CompletableFuture;

@Configuration
public class AppConfig {

    @Bean
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUsername("apiUser");
        dataSource.setPassword("apiUser");
        dataSource.setUrl("jdbc:mysql://172.16.128.128:3306/CodeEditor");
        return dataSource;
    }

    @Bean
    public JdbcTemplate getDatabaseContext(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public IUserRepository getUserRepository(JdbcTemplate context) {
        return new UserRepository(context);
    }

    @Bean
    public UserLogic getUserLogic(IUserRepository userRepo) {
        return new UserLogic(userRepo);
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public HttpClient httpClient() {
        return HttpClient.newBuilder()
                .followRedirects(HttpClient.Redirect.ALWAYS)
                .version(HttpClient.Version.HTTP_1_1)
                .build();
    }

    @Bean
    public PublicKey publicKey(HttpLogic httpLogic) throws IOException, URISyntaxException, NoSuchAlgorithmException, InvalidKeySpecException {
        CompletableFuture<HttpResponse<PublicKeyResponse>> future = httpLogic.requestAsync(Endpoints.AUTHSERVER + "/auth/key", HttpMethod.GET, PublicKeyResponse.class);
        HttpResponse<PublicKeyResponse> response = future.join();
        PublicKeyResponse keyResponse = response.body();
        String base64Key = keyResponse.getPublicKey();
        byte[] keyBytes = Base64.getDecoder().decode(base64Key);
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(keyBytes, "RSA"));
    }

    @Bean
    public HttpLogic httpLogic(ObjectMapper objectMapper, HttpClient client) {
        return new HttpLogic(objectMapper, client);
    }

    @Bean
    public AuthLogic authLogic(HttpLogic httpLogic, IUserRepository userRepo, ObjectMapper objectMapper, PublicKey key) {
        return new AuthLogic(httpLogic, userRepo, objectMapper, key);
    }

    @Bean
    public IProjectRepository projectRepository(JdbcTemplate dbCOntext) {
        return new ProjectRepository(dbCOntext);
    }

    @Bean
    public IProjectLogic projectLogic(IProjectRepository projectRepo, IBufferedFileRepository fileRepo) {
        return new ProjectLogic(projectRepo, fileRepo);
    }

    @Bean
    public IFileRepository fileRepository() {
        return new FileRepository("files");
    }

    @Bean
    public IBufferedFileRepository bufferedFileRepository(IFileRepository innerRepo) {
        return new BufferedFileRepository(innerRepo);
    }

}
