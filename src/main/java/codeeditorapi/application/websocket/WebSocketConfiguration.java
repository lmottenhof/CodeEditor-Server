package codeeditorapi.application.websocket;

import codeeditorapi.data.interfaces.IBufferedFileRepository;
import codeeditorapi.logic.interfaces.IAuthLogic;
import codeeditorapi.logic.interfaces.IProjectLogic;
import codeeditorapi.logic.interfaces.IUserLogic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean;

@Configuration
@EnableWebSocket
public class WebSocketConfiguration implements WebSocketConfigurer {
    private IAuthLogic authLogic;
    private IProjectLogic projectLogic;
    private IBufferedFileRepository fileRepo;
    private IUserLogic userLogic;

    public WebSocketConfiguration(IAuthLogic authLogic, IProjectLogic projectLogic, IUserLogic userLogic, IBufferedFileRepository fileRepo) {
        this.authLogic = authLogic;
        this.projectLogic = projectLogic;
        this.userLogic = userLogic;
        this.fileRepo = fileRepo;
    }


    @Bean
    public ServletServerContainerFactoryBean createWebSocketContainer() {
        ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
        container.setMaxBinaryMessageBufferSize(1024000);
        return container;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        WebSocketHandler socket = new WebSocketHandler(this.authLogic, this.projectLogic, this.userLogic);

        TextHandler textHandler = new TextHandler(socket, this.fileRepo);
        ChatHandler chatHandler = new ChatHandler(socket);
        socket.registerHandler(WebSocketHandler.TEXTHANDLER, textHandler::handleMessage);
        socket.registerHandler(WebSocketHandler.CHATHANDLER, chatHandler::handleMessage);
        registry.addHandler(socket, "/socket/**").addInterceptors(new WebSocketHandshakeInterceptor(this.authLogic)).setAllowedOrigins("*");

    }
}
