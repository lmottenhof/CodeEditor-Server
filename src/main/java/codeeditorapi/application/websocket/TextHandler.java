package codeeditorapi.application.websocket;

import codeeditorapi.application.websocket.messages.TextEditMessage;
import codeeditorapi.application.websocket.messages.TextInsertMessage;
import codeeditorapi.application.websocket.messages.TextRemoveMessage;
import codeeditorapi.data.interfaces.IBufferedFileRepository;
import codeeditorapi.shared.exceptions.ResourceNotFoundException;
import codeeditorapi.shared.models.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TextHandler {
    private IBufferedFileRepository fileRepo;
    private WebSocketHandler socket;
    private Map<UserModel, String> openFiles = new HashMap<>();
    private Map<Byte, MessageHandler> innerMessageHandlers = new HashMap<>();
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public TextHandler(WebSocketHandler socket, IBufferedFileRepository fileRepo) {
        this.fileRepo = fileRepo;
        this.socket = socket;

        this.innerMessageHandlers.put((byte) 0, this::insertText);
        this.innerMessageHandlers.put((byte) 1, this::removeText);
        this.innerMessageHandlers.put((byte) 2, this::setUserFile);


    }

    public void handleMessage(MessageReceivedEventArgs args) throws IOException {
        this.innerMessageHandlers.get(args.getMessage().getBytes()[1]).handle(args);
    }

    private void setUserFile(MessageReceivedEventArgs args) throws IOException {
        String fileName = new String(Arrays.copyOfRange(args.getMessage().getBytes(), 2, args.getMessage().getBytes().length));
        this.openFiles.remove(args.getSender());
        this.openFiles.put(args.getSender(), fileName);
        if(!this.openFiles.containsValue(fileName)) {
            this.fileRepo.close(fileName);
        }
    }

    private void insertText(MessageReceivedEventArgs args) throws IOException {
        TextInsertMessage message = new TextInsertMessage(args.getMessage());
        String path = String.format("%s/%s", args.getProject().getProjectRoot(), this.openFiles.get(args.getSender()));
        if(this.openFiles.containsKey(args.getSender())) {
            try {
                this.fileRepo.write(path, message.getEditPosition(), message.getInsertedText());
            } catch(ResourceNotFoundException ex) {
                this.logger.info(String.format("Failed to write to file at %s for %s", path, args.getSender().getName()), ex);
                this.openFiles.remove(args.getSender());
            }

        }
        sendChanges(args, message);
    }

    private void removeText(MessageReceivedEventArgs args) throws IOException {
        TextRemoveMessage message = new TextRemoveMessage(args.getMessage());
        String path = String.format("%s/%s", args.getProject().getProjectRoot(), this.openFiles.get(args.getSender()));
        if(this.openFiles.containsKey(args.getSender())) {
            try {
                this.fileRepo.remove(path, message.getEditPosition(), message.getRemovedTextLength());
            } catch(ResourceNotFoundException ex) {
                this.openFiles.remove(args.getSender());
            }

        }
        sendChanges(args, message);
    }

    private void sendChanges(MessageReceivedEventArgs args, TextEditMessage message) throws IOException {
        List<UserModel> users = args.getUsers();
        for (UserModel user : users) {
            if (user.equals(args.getSender())) {
                continue;
            }
            if(this.openFiles.containsKey(user)) {
                if(this.openFiles.get(user).equals(this.openFiles.get(args.getSender()))) {
                    socket.sendMessage(user, message);
                }
            }
        }
    }
}