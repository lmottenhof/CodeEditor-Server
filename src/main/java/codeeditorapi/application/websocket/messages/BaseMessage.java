package codeeditorapi.application.websocket.messages;

public class BaseMessage {
    protected byte[] message;

    public BaseMessage(byte[] message) {
        this.message = message;
    }

    public byte getMessageType() {
        return this.message[0];
    }

    public byte[] getBytes() {
        return this.message;
    }
}
