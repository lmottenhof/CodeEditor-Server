package codeeditorapi.application.websocket.messages;

import java.util.Arrays;

public class TextInsertMessage extends TextEditMessage {

    public TextInsertMessage(BaseMessage message) {
        super(message);
    }

    public String getInsertedText() {
        return new String(Arrays.copyOfRange(this.message, 6, this.message.length));
    }


}
