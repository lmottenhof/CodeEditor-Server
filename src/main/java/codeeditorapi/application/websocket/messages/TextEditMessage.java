package codeeditorapi.application.websocket.messages;

import codeeditorapi.application.websocket.ByteUtil;

public class TextEditMessage extends BaseMessage{

    public TextEditMessage(BaseMessage message) {
        super(message.message);
    }


    public byte getEditType() {
        return this.message[1];
    }

    public int getEditPosition() {
        return ByteUtil.readInt(this.message, 2, 4);
    }
}
