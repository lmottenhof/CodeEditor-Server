package codeeditorapi.application.websocket.messages;

import lombok.Getter;

public class ChatMessage extends BaseMessage{
    @Getter
    private String sender;
    @Getter
    private String content;

    public ChatMessage(String sender, String content) {
        super(new byte[4 + sender.length() + content.length()]);
        this.sender = sender;
        this.content = content;
    }

    @Override
    public byte[] getBytes() {
        message[0] = 0;
        message[1] = 0;
        message[2] = (byte)sender.length();
        for (int i = 0; i < sender.length(); i++) {
            message[3 + i] = ((byte) sender.charAt(i));
        }

        message[3 + sender.length()] = (byte)content.length();

        for (int i = 0; i < content.length(); i++) {
            message[4 + sender.length() + i] = ((byte) content.charAt(i));
        }
        return message;
    }
}
