package codeeditorapi.application.websocket.messages;

public class TextRemoveMessage extends TextEditMessage {

    public TextRemoveMessage(BaseMessage message) {
        super(message);
    }

    public int getRemovedTextLength() {
        return this.message[6];
    }
}
