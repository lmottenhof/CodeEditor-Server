package codeeditorapi.application.websocket;

import codeeditorapi.application.websocket.messages.ChatMessage;
import codeeditorapi.shared.models.UserModel;

import java.io.IOException;
import java.util.Arrays;

public class ChatHandler {
    private WebSocketHandler socket;

    public ChatHandler(WebSocketHandler socket) {
        this.socket = socket;
    }

    public void handleMessage(MessageReceivedEventArgs args) throws IOException {
        ChatMessage message = new ChatMessage(args.getSender().getName(), new String(Arrays.copyOfRange(args.getMessage().getBytes(), 3, args.getMessage().getBytes().length)));

        for (UserModel user : args.getUsers()) {
            socket.sendMessage(user, message);
        }
    }
}
