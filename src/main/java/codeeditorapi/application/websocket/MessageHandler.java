package codeeditorapi.application.websocket;

import java.io.IOException;

@FunctionalInterface
public interface MessageHandler {

    void handle(MessageReceivedEventArgs message) throws IOException;
}
