package codeeditorapi.application.websocket;

import codeeditorapi.application.websocket.messages.BaseMessage;
import codeeditorapi.logic.interfaces.IAuthLogic;
import codeeditorapi.logic.interfaces.IProjectLogic;
import codeeditorapi.logic.interfaces.IUserLogic;
import codeeditorapi.shared.TokenData;
import codeeditorapi.shared.exceptions.AuthenticationException;
import codeeditorapi.shared.models.UserModel;
import codeeditorapi.shared.responses.ProjectDetailResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebSocketHandler extends AbstractWebSocketHandler {
    public static final byte CHATHANDLER = 0;
    public static final byte TEXTHANDLER = 1;

    private final IAuthLogic authLogic;
    private final IProjectLogic projectLogic;
    private final IUserLogic userLogic;

    private final List<WebSocketSession> sessions = new ArrayList<>();
    private final Map<WebSocketSession, UserModel> authenticatedUsers = new HashMap<>();
    private final Map<UserModel, WebSocketSession> userSessions = new HashMap<>();
    private final Map<WebSocketSession, ProjectDetailResponse> userProjects = new HashMap<>();
    private final MultiValueMap<ProjectDetailResponse, UserModel> projectUsers = new LinkedMultiValueMap<>();

    private final Map<Byte, MessageHandler> handlers = new HashMap<>();

    public WebSocketHandler(IAuthLogic authLogic, IProjectLogic projectLogic, IUserLogic userLogic) {
        this.authLogic = authLogic;
        this.projectLogic = projectLogic;
        this.userLogic = userLogic;
    }


    @Override
    public void afterConnectionEstablished(WebSocketSession newSession) {
        sessions.add(newSession);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        UserModel user = this.authenticatedUsers.get(session);
        ProjectDetailResponse project = userProjects.get(session);
        projectUsers.get(project).remove(user);
        if (projectUsers.get(project).isEmpty()) {
            projectUsers.remove(project);
        }
        authenticatedUsers.remove(session);
        userProjects.remove(session);
        userSessions.remove(user);
        sessions.remove(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws IOException {
        if (!this.authenticatedUsers.containsKey(session)) {
           verifyUser(message, session);
           addUserToProject(session);
        }
    }

    private void verifyUser(TextMessage message, WebSocketSession session) throws IOException {
        TokenData tokenData;
        try {
            tokenData = this.authLogic.verify(message.getPayload());
            UserModel user = this.userLogic.getUserById(tokenData.getId());
            this.authenticatedUsers.put(session, user);
            this.userSessions.put(user, session);
        } catch (AuthenticationException ex) {
            session.sendMessage(new TextMessage("Invalid token"));
            session.close();
        }
    }

    private void addUserToProject(WebSocketSession session) throws IOException {
        UserModel user = this.authenticatedUsers.get(session);
        String projectPath = (String)session.getAttributes().get("projectRoot");
        String[] projectLocation = (projectPath).split("/");

        try {
            ProjectDetailResponse project = this.projectLogic.getProjectByName(projectLocation[1], projectLocation[0], user.getId());
            this.userProjects.put(session, project);
            this.projectUsers.add(project, user);
        } catch (AuthenticationException ex) {
            session.sendMessage(new TextMessage("Project not found"));
            session.close();
        }
    }

    @Override
    protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws IOException {
        if (!this.authenticatedUsers.containsKey(session)) {
            session.sendMessage(new TextMessage("Send token first"));
            return;
        }

        if (!this.userProjects.containsKey(session)) {
            session.sendMessage(new TextMessage("Select project first"));
            return;
        }

        handleMessage(session, message.getPayload().array());
    }

    public WebSocketHandler registerHandler(byte type, MessageHandler handler) {
        if (handlers.containsKey(type)) {
            throw new IllegalStateException(String.format("Message type of %s already in use", handler.toString()));
        }
        handlers.put(type, handler);
        return this;
    }

    private void handleMessage(WebSocketSession session, byte[] buffer) throws IOException {
        BaseMessage message = new BaseMessage(buffer);
        ProjectDetailResponse project = this.userProjects.get(session);

        MessageReceivedEventArgs args = new MessageReceivedEventArgs(message,
                this.authenticatedUsers.get(session),
                this.projectUsers.get(project),
                project );
        this.handlers.get(message.getMessageType()).handle(args);
    }

    public void sendMessage(UserModel user, BaseMessage message) throws IOException {
        this.userSessions.get(user).sendMessage(new BinaryMessage(message.getBytes()));
    }

}