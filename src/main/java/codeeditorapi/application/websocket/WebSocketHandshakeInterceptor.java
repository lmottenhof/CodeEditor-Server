package codeeditorapi.application.websocket;

import codeeditorapi.logic.interfaces.IAuthLogic;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;

public class WebSocketHandshakeInterceptor implements HandshakeInterceptor {

    public WebSocketHandshakeInterceptor(IAuthLogic authLogic) {

    }

    @Override
    public boolean beforeHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Map<String, Object> attributes) throws Exception {
        String path =  serverHttpRequest.getURI().getPath();
        String[] split = path.split("/");
        String projectRoot =  path.substring(path.indexOf(split[split.length-2]));
        attributes.put("projectRoot", projectRoot);
        return true;
    }

    @Override
    public void afterHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Exception e) {
        //Nothing needs to be done after the handshake as of now, it it safe to ignore this.
    }
}
