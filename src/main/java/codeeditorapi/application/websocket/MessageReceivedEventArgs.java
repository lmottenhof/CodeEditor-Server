package codeeditorapi.application.websocket;

import codeeditorapi.application.websocket.messages.BaseMessage;
import codeeditorapi.shared.models.UserModel;
import codeeditorapi.shared.responses.ProjectDetailResponse;
import lombok.Data;
import lombok.Getter;

import java.util.List;

@Data
public class MessageReceivedEventArgs {
//    public MessageReceivedEventArgs() {
//
//    }

    private final BaseMessage message;
    private final UserModel sender;
    private final List<UserModel> users;
    private final ProjectDetailResponse project;

}
