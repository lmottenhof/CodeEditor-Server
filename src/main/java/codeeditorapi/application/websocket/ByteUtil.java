package codeeditorapi.application.websocket;

import java.nio.ByteBuffer;

public class ByteUtil {
    private ByteUtil() {
        //Empty private constructor to make instantiation impossible
    }

    public static int readInt(byte[] bytes, int offset, int length) {
        return ByteBuffer.wrap(bytes, offset, length).getInt();
    }
}
