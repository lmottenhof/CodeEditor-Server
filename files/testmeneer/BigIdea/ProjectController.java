package codeeditorapi.application.controllers;

import codeeditorapi.logic.interfaces.IProjectLogic;
import codeeditorapi.shared.TokenData;
import codeeditorapi.shared.exceptions.AuthenticationException;
import codeeditorapi.shared.exceptions.ResourceAlreadyExistsException;
import codeeditorapi.shared.exceptions.ResourceNotFoundException;
import codeeditorapi.shared.requests.NewProjectRequest;
import codeeditorapi.shared.responses.FileDataResponse;
import codeeditorapi.shared.responses.NewProjectResponse;
import codeeditorapi.shared.responses.ProjectDetailResponse;
import codeeditorapi.shared.responses.ProjectResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/project")
@CrossOrigin("*")
public class ProjectController {
    private IProjectLogic projectLogic;

    public ProjectController(IProjectLogic projectLogic) {
        this.projectLogic = projectLogic;
    }

    @PostMapping("/new")
    public ResponseEntity<NewProjectResponse> newProject(@RequestAttribute("user") TokenData user, @RequestBody NewProjectRequest request) {
        String projectId;
        try {
            projectId = this.projectLogic.newProject(user.getId(), request.getProjectName(), user.getName());
        } catch (ResourceAlreadyExistsException ex) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        NewProjectResponse response = new NewProjectResponse();
        response.setId(projectId);
        response.setName(request.getProjectName());
        response.setUserName(user.getName());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{userName}/{projectName}")
    public ResponseEntity<ProjectDetailResponse> getProject(@PathVariable("projectName") String projectName, @PathVariable("userName") String userName, @RequestAttribute("user") TokenData user) {
        ProjectDetailResponse project;
        try {
            project = this.projectLogic.getProjectByName(projectName, userName, user.getId());
        } catch (AuthenticationException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<List<ProjectResponse>> getProjects(@RequestAttribute("user") TokenData user) {
        List<ProjectResponse> projects;
        try {
            projects = this.projectLogic.getProjectsOfUser(user.getId());
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(projects, HttpStatus.OK);
    }

    @GetMapping("/{userName}/{projectName}/{fileName}")
    public ResponseEntity<FileDataResponse> getFile(@RequestAttribute("user") TokenData user, @PathVariable("projectName") String projectName, @PathVariable("userName") String userName, @PathVariable("fileName") String fileName) {
        try {
            FileDataResponse response = this.projectLogic.getFile(projectName, userName, fileName, user.getId());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (ResourceNotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/{userName}/{projectName}/{path}")
    public ResponseEntity uploadFile(@RequestAttribute("user") TokenData user, @PathVariable("projectName") String projectName, @PathVariable("userName") String userName, @PathVariable("path") String path, @RequestParam("file") MultipartFile file) {
        try {
            this.projectLogic.saveFile(file.getBytes(), projectName, userName, path, user.getId());
            return new ResponseEntity(HttpStatus.OK);
        } catch(Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }
}
